﻿namespace Synapse.Vues
{
    partial class FromNouveauIntervenant
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonValiderIntervenant = new System.Windows.Forms.Button();
            this.textBoxTauxHoraire = new System.Windows.Forms.TextBox();
            this.labelTauxHoraire = new System.Windows.Forms.Label();
            this.textBoxNom = new System.Windows.Forms.TextBox();
            this.labelNom = new System.Windows.Forms.Label();
            this.labelPrincipal = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonValiderIntervenant);
            this.groupBox1.Controls.Add(this.textBoxTauxHoraire);
            this.groupBox1.Controls.Add(this.labelTauxHoraire);
            this.groupBox1.Controls.Add(this.textBoxNom);
            this.groupBox1.Controls.Add(this.labelNom);
            this.groupBox1.Controls.Add(this.labelPrincipal);
            this.groupBox1.Location = new System.Drawing.Point(14, 14);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(256, 234);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // buttonValiderIntervenant
            // 
            this.buttonValiderIntervenant.Location = new System.Drawing.Point(6, 184);
            this.buttonValiderIntervenant.Name = "buttonValiderIntervenant";
            this.buttonValiderIntervenant.Size = new System.Drawing.Size(244, 44);
            this.buttonValiderIntervenant.TabIndex = 6;
            this.buttonValiderIntervenant.Text = "Valider";
            this.buttonValiderIntervenant.UseVisualStyleBackColor = true;
            this.buttonValiderIntervenant.Click += new System.EventHandler(this.buttonValiderIntervenant_Click);
            // 
            // textBoxTauxHoraire
            // 
            this.textBoxTauxHoraire.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.textBoxTauxHoraire.Location = new System.Drawing.Point(6, 138);
            this.textBoxTauxHoraire.Name = "textBoxTauxHoraire";
            this.textBoxTauxHoraire.Size = new System.Drawing.Size(244, 23);
            this.textBoxTauxHoraire.TabIndex = 5;
            this.textBoxTauxHoraire.Leave += new System.EventHandler(this.textBoxTauxHoraire_Leave);
            // 
            // labelTauxHoraire
            // 
            this.labelTauxHoraire.AutoSize = true;
            this.labelTauxHoraire.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.labelTauxHoraire.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.labelTauxHoraire.Location = new System.Drawing.Point(83, 118);
            this.labelTauxHoraire.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelTauxHoraire.Name = "labelTauxHoraire";
            this.labelTauxHoraire.Size = new System.Drawing.Size(90, 17);
            this.labelTauxHoraire.TabIndex = 4;
            this.labelTauxHoraire.Text = "Taux Horaire";
            // 
            // textBoxNom
            // 
            this.textBoxNom.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.textBoxNom.Location = new System.Drawing.Point(6, 80);
            this.textBoxNom.Name = "textBoxNom";
            this.textBoxNom.Size = new System.Drawing.Size(244, 23);
            this.textBoxNom.TabIndex = 3;
            this.textBoxNom.Leave += new System.EventHandler(this.textBoxNom_Leave);
            // 
            // labelNom
            // 
            this.labelNom.AutoSize = true;
            this.labelNom.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.labelNom.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.labelNom.Location = new System.Drawing.Point(110, 60);
            this.labelNom.Margin = new System.Windows.Forms.Padding(5, 0, 5, 0);
            this.labelNom.Name = "labelNom";
            this.labelNom.Size = new System.Drawing.Size(37, 17);
            this.labelNom.TabIndex = 2;
            this.labelNom.Text = "Nom";
            // 
            // labelPrincipal
            // 
            this.labelPrincipal.AutoSize = true;
            this.labelPrincipal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.labelPrincipal.Location = new System.Drawing.Point(48, 16);
            this.labelPrincipal.Name = "labelPrincipal";
            this.labelPrincipal.Size = new System.Drawing.Size(161, 20);
            this.labelPrincipal.TabIndex = 1;
            this.labelPrincipal.Text = "Nouveau Intervenant";
            // 
            // FromNouveauIntervenant
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.groupBox1);
            this.Name = "FromNouveauIntervenant";
            this.Text = "FromNouveauIntervenant";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label labelPrincipal;
        private System.Windows.Forms.Label labelNom;
        private System.Windows.Forms.TextBox textBoxNom;
        private System.Windows.Forms.TextBox textBoxTauxHoraire;
        private System.Windows.Forms.Label labelTauxHoraire;
        private System.Windows.Forms.Button buttonValiderIntervenant;
    }
}