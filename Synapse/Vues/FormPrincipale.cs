﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Synapse.Utilitaire;
using Synapse.Metiers;

namespace Synapse.Vues
{
    public partial class FormPrincipale : Form
    {
        private Form _mdiChild;

        private Form MdiChild
        {
            get { return _mdiChild; }
            set
            {
                if (_mdiChild != null)
                {
                    _mdiChild.Dispose();
                }
                _mdiChild = value;
                _mdiChild.MdiParent = value.MdiParent;
                _mdiChild.MaximumSize = _mdiChild.Size;
                _mdiChild.MinimumSize = _mdiChild.Size;
                _mdiChild.Show();
            }
        }

        public FormPrincipale()
        {
            InitializeComponent();
        }

        private void FormPrincipale_Load(object sender, EventArgs e)
        {
            Data.ChargeDeDonnees();
        }

        private void FormPrincipale_FormClosed(object sender, FormClosedEventArgs e)
        {
            Data.SauvegardeDonnees();
        }

        private void reinitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Data.CollectionProjet.Clear();
            Data.CollectionIntervenant.Clear();
        }

        private void nouvelIntervenantToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiChild = new FromNouveauIntervenant();
        }

        private void visualiserTousLesIntervenantToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiChild = new FormVisualisationIntervenant();
        }

        private void rechercheIntervenantToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiChild = new FormRechercheIntervenant();
        }
    }
}
