﻿using System;
using System.Windows.Forms;
using Synapse.Utilitaire;
using Synapse.Metiers;

namespace Synapse.Vues
{
    public partial class FromNouveauIntervenant : Form
    {
        public FromNouveauIntervenant()
        {
            InitializeComponent();
        }

        private void ReinitForm()
        {
            textBoxNom.Text = "";
            textBoxTauxHoraire.Text = "";

            textBoxNom.Focus();
        }


        private void textBoxNom_Leave(object sender, EventArgs e)
        {
            if (textBoxNom.Text.Length >= 3)
            {
                if(!Formulaire.VerificationFormatNomIntervenant(textBoxNom.Text))
                {
                    MessageBox.Show("Erreur de saisie");
                    textBoxNom.Focus();
                }
            }
        }

        private void textBoxTauxHoraire_Leave(object sender, EventArgs e)
        {
            if (textBoxNom.Text.Length >= 4)
            {
                switch (Formulaire.VerificationFormatTauxHoraire(textBoxTauxHoraire.Text))
                {
                    case 0:
                        MessageBox.Show("Erreur de saisie");
                        textBoxTauxHoraire.Focus();
                        break;
                    case 1:
                        break;
                    case 2:
                        MessageBox.Show("Modifier le point en virgule");
                        textBoxTauxHoraire.Focus();
                        break;
                }
            }
        }

        private void buttonValiderIntervenant_Click(object sender, EventArgs e)
        {
            string nom;
            decimal tauxHoraire;

            nom = textBoxNom.Text;
            tauxHoraire = Convert.ToDecimal(textBoxTauxHoraire.Text);
            intervenant nouveauIntervenant = new intervenant(nom,tauxHoraire);

            Data.CollectionIntervenant.Add(nouveauIntervenant);
            MessageBox.Show("L'intervenant à été ajouter.");
            ReinitForm();
        }
    }
}
