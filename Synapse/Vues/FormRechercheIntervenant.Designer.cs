﻿namespace Synapse.Vues
{
    partial class FormRechercheIntervenant
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelPrincipal = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.groupBoxRecherche = new System.Windows.Forms.GroupBox();
            this.labelRecherche = new System.Windows.Forms.Label();
            this.textBoxNomIntervenant = new System.Windows.Forms.TextBox();
            this.textBoxTauxHoraire = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBoxRecherche.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelPrincipal
            // 
            this.labelPrincipal.AutoSize = true;
            this.labelPrincipal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.labelPrincipal.Location = new System.Drawing.Point(19, 16);
            this.labelPrincipal.Name = "labelPrincipal";
            this.labelPrincipal.Size = new System.Drawing.Size(228, 20);
            this.labelPrincipal.TabIndex = 1;
            this.labelPrincipal.Text = "Visualisation des Intervenant ";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dataGridView1);
            this.groupBox1.Controls.Add(this.labelPrincipal);
            this.groupBox1.Location = new System.Drawing.Point(14, 109);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(256, 139);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(6, 39);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.Size = new System.Drawing.Size(241, 94);
            this.dataGridView1.TabIndex = 2;
            // 
            // groupBoxRecherche
            // 
            this.groupBoxRecherche.Controls.Add(this.label2);
            this.groupBoxRecherche.Controls.Add(this.label1);
            this.groupBoxRecherche.Controls.Add(this.textBoxTauxHoraire);
            this.groupBoxRecherche.Controls.Add(this.textBoxNomIntervenant);
            this.groupBoxRecherche.Controls.Add(this.labelRecherche);
            this.groupBoxRecherche.Location = new System.Drawing.Point(14, 12);
            this.groupBoxRecherche.Name = "groupBoxRecherche";
            this.groupBoxRecherche.Size = new System.Drawing.Size(247, 89);
            this.groupBoxRecherche.TabIndex = 3;
            this.groupBoxRecherche.TabStop = false;
            // 
            // labelRecherche
            // 
            this.labelRecherche.AutoSize = true;
            this.labelRecherche.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F);
            this.labelRecherche.Location = new System.Drawing.Point(40, 16);
            this.labelRecherche.Name = "labelRecherche";
            this.labelRecherche.Size = new System.Drawing.Size(177, 20);
            this.labelRecherche.TabIndex = 0;
            this.labelRecherche.Text = "Recherche Intervenant";
            // 
            // textBoxNomIntervenant
            // 
            this.textBoxNomIntervenant.Location = new System.Drawing.Point(6, 60);
            this.textBoxNomIntervenant.Name = "textBoxNomIntervenant";
            this.textBoxNomIntervenant.Size = new System.Drawing.Size(100, 20);
            this.textBoxNomIntervenant.TabIndex = 1;
            this.textBoxNomIntervenant.Leave += new System.EventHandler(this.textBoxNomIntervenant_Leave);
            // 
            // textBoxTauxHoraire
            // 
            this.textBoxTauxHoraire.Location = new System.Drawing.Point(141, 60);
            this.textBoxTauxHoraire.Name = "textBoxTauxHoraire";
            this.textBoxTauxHoraire.Size = new System.Drawing.Size(100, 20);
            this.textBoxTauxHoraire.TabIndex = 2;
            this.textBoxTauxHoraire.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Nom Intervenant";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(157, 44);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Taux Horaire";
            // 
            // FormRechercheIntervenant
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.groupBoxRecherche);
            this.Controls.Add(this.groupBox1);
            this.Name = "FormRechercheIntervenant";
            this.Text = "FormRechercheIntervenant";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBoxRecherche.ResumeLayout(false);
            this.groupBoxRecherche.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Label labelPrincipal;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.GroupBox groupBoxRecherche;
        private System.Windows.Forms.Label labelRecherche;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxTauxHoraire;
        private System.Windows.Forms.TextBox textBoxNomIntervenant;
    }
}