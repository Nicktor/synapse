﻿namespace Synapse.Vues
{
    partial class FormPrincipale
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.intervenantToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nouvelIntervenantToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.visualiserTousLesIntervenantToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rechercheIntervenantToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.projetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nouveauProjetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.visualiserTousLesProjetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rechercheProjetToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.reinitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.intervenantToolStripMenuItem,
            this.projetToolStripMenuItem,
            this.reinitToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(284, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // intervenantToolStripMenuItem
            // 
            this.intervenantToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nouvelIntervenantToolStripMenuItem,
            this.visualiserTousLesIntervenantToolStripMenuItem,
            this.rechercheIntervenantToolStripMenuItem});
            this.intervenantToolStripMenuItem.Name = "intervenantToolStripMenuItem";
            this.intervenantToolStripMenuItem.Size = new System.Drawing.Size(79, 20);
            this.intervenantToolStripMenuItem.Text = "Intervenant";
            // 
            // nouvelIntervenantToolStripMenuItem
            // 
            this.nouvelIntervenantToolStripMenuItem.Name = "nouvelIntervenantToolStripMenuItem";
            this.nouvelIntervenantToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.nouvelIntervenantToolStripMenuItem.Text = "Nouvel Intervenant";
            this.nouvelIntervenantToolStripMenuItem.Click += new System.EventHandler(this.nouvelIntervenantToolStripMenuItem_Click);
            // 
            // visualiserTousLesIntervenantToolStripMenuItem
            // 
            this.visualiserTousLesIntervenantToolStripMenuItem.Name = "visualiserTousLesIntervenantToolStripMenuItem";
            this.visualiserTousLesIntervenantToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.visualiserTousLesIntervenantToolStripMenuItem.Text = "Visualiser tous les Intervenant";
            this.visualiserTousLesIntervenantToolStripMenuItem.Click += new System.EventHandler(this.visualiserTousLesIntervenantToolStripMenuItem_Click);
            // 
            // rechercheIntervenantToolStripMenuItem
            // 
            this.rechercheIntervenantToolStripMenuItem.Name = "rechercheIntervenantToolStripMenuItem";
            this.rechercheIntervenantToolStripMenuItem.Size = new System.Drawing.Size(229, 22);
            this.rechercheIntervenantToolStripMenuItem.Text = "Recherche Intervenant";
            this.rechercheIntervenantToolStripMenuItem.Click += new System.EventHandler(this.rechercheIntervenantToolStripMenuItem_Click);
            // 
            // projetToolStripMenuItem
            // 
            this.projetToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nouveauProjetToolStripMenuItem,
            this.visualiserTousLesProjetToolStripMenuItem,
            this.rechercheProjetToolStripMenuItem});
            this.projetToolStripMenuItem.Name = "projetToolStripMenuItem";
            this.projetToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.projetToolStripMenuItem.Text = "Projet ";
            // 
            // nouveauProjetToolStripMenuItem
            // 
            this.nouveauProjetToolStripMenuItem.Name = "nouveauProjetToolStripMenuItem";
            this.nouveauProjetToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.nouveauProjetToolStripMenuItem.Text = "Nouveau Projet";
            // 
            // visualiserTousLesProjetToolStripMenuItem
            // 
            this.visualiserTousLesProjetToolStripMenuItem.Name = "visualiserTousLesProjetToolStripMenuItem";
            this.visualiserTousLesProjetToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.visualiserTousLesProjetToolStripMenuItem.Text = "Visualiser tous les projet";
            // 
            // rechercheProjetToolStripMenuItem
            // 
            this.rechercheProjetToolStripMenuItem.Name = "rechercheProjetToolStripMenuItem";
            this.rechercheProjetToolStripMenuItem.Size = new System.Drawing.Size(200, 22);
            this.rechercheProjetToolStripMenuItem.Text = "Recherche projet";
            // 
            // reinitToolStripMenuItem
            // 
            this.reinitToolStripMenuItem.Name = "reinitToolStripMenuItem";
            this.reinitToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.reinitToolStripMenuItem.Text = "Reinit";
            this.reinitToolStripMenuItem.Click += new System.EventHandler(this.reinitToolStripMenuItem_Click);
            // 
            // FormPrincipale
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FormPrincipale";
            this.Text = "FormPrincipale";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormPrincipale_FormClosed);
            this.Load += new System.EventHandler(this.FormPrincipale_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem intervenantToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nouvelIntervenantToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem visualiserTousLesIntervenantToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rechercheIntervenantToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem projetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nouveauProjetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem visualiserTousLesProjetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem rechercheProjetToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem reinitToolStripMenuItem;
    }
}