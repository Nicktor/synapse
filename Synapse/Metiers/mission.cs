﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synapse.Metiers
{
    [Serializable]
    class mission
    {
        #region Varable private

        private string _nom;
        private string _description;
        private int _nbHeuresPrevues;
        private Dictionary<DateTime, int> _releveHoraire;
        private intervenant _intervenant;

        #endregion

        #region Acesseurs 

        public Dictionary<DateTime, int> ReleveHoraire
        {
            get
            {
                return _releveHoraire;
            }
        }

        internal intervenant Intervenant
        {
            get
            {
                return _intervenant;
            }
        }

        #endregion

        #region Mutateur

        #endregion

        #region Méthode

        public int NbHeuresEffectuees()
        {
            int resultat = 0;

            foreach  (int nbHeure in _releveHoraire.Values)
            {
                resultat+= nbHeure;
            }
            return resultat;
        }

        public void AjoutRekeve(DateTime jour, int nbHeures)
        {
            _releveHoraire.Add(jour, nbHeures);
        }

        #endregion
    }
}
