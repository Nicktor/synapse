﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synapse.Metiers
{
    [Serializable]
    class intervenant
    {
        #region Variable private

        private string _nom;
        private decimal _tauxHoraire;



        #endregion

        #region Constructeur

        public intervenant(string nom, decimal tauxHoraire)
        {
            _nom = nom;
            _tauxHoraire = tauxHoraire;
        }




        #endregion

        #region Acesseurs

        public string Nom
        {
            get
            {
                return _nom;
            }
        }
        public decimal TauxHoraire
        {
            get
            {
                return _tauxHoraire;
            }
        }

        #endregion

        #region Mutateur

        #endregion

        #region Méthode

        #endregion
    }
}
