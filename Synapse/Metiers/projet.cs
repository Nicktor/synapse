﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Synapse.Metiers
{
    [Serializable]
    class projet
    {
        #region Varable private

        private string _nom;
        private DateTime _debut;
        private DateTime _fin;
        private decimal _prixFactureMO;

        private List<mission> _listMission;

        #endregion

        #region Acesseurs 

        #endregion

        #region Mutateur

        #endregion

        #region Méthode

        private decimal CumulCoutMO()
        {
            decimal resultat = 0;

            foreach (mission missionCourante in _listMission)
            {
                resultat = resultat + (missionCourante.NbHeuresEffectuees() * missionCourante.Intervenant.TauxHoraire); 
            }

            return resultat;
        }

        public object MargeBruteCourante()
        {
            decimal resultat = 0;

            resultat = _prixFactureMO - CumulCoutMO();

            return resultat;
        }
        #endregion 
    }
}
