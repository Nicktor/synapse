﻿using System;
using System.Text.RegularExpressions;

namespace Synapse.Utilitaire
{
    class Formulaire
    {
        // Retourne 0 si faux, 1 si vrai , 2 si il faut changer le point a la place de la vrigule.
        public static int VerificationFormatTauxHoraire(string tauxHoraire)
        {
            int resultat = 0;
            Regex formatTauxHoraireVirg = new Regex(@"^([0-9]{1,})\,([0-9]{2})$");
            Regex formatTauxHorairePoint = new Regex(@"^([0-9]{1,})\.([0-9]{2})$");

            if (formatTauxHoraireVirg.IsMatch(tauxHoraire))
                resultat = 1;
            else if (formatTauxHorairePoint.IsMatch(tauxHoraire))
                resultat = 2;
            return resultat;                
        }

        public static bool VerificationFormatNomIntervenant(string nom)
        {
            bool resultat = false;
            Regex formatNom = new Regex(@"^([A-Z])([a-z]{2,20})$");
            if (formatNom.IsMatch(nom))
                resultat = true;
            return resultat;
        }
    }
}
