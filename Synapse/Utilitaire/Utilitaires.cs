﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;



namespace Synapse.Utilitaire
{

    class Utilitaires
    {
        private static string _repertoireApplication = Environment.CurrentDirectory + @"\";

        public static IList ChargeDonnees(string nomFichier)
        {
            FileStream fs = null;
            IList listeItem = null;

            try
            {
                fs = new FileStream(_repertoireApplication + nomFichier, FileMode.Open);
                BinaryFormatter formatter = new BinaryFormatter();
                try
                {
                    listeItem = (IList)formatter.Deserialize(fs);
                }
                catch (Exception err)
                {
                    FichierLog(err);                 
                }
                finally
                {
                    fs.Close();
                }
            }
            catch (Exception) {}
            return listeItem;
        }


        public static void SauvegarderDonnees(string nomFichier, IList collection)
        {
            FileStream file = null;

            try
            {
                file = File.Open(_repertoireApplication + nomFichier, FileMode.OpenOrCreate);
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(file, collection);
            }
            catch (Exception err)
            {
                FichierLog(err);
            }
            finally
            {
                if (file != null)
                    file.Close();
            }
        }


        private static void FichierLog(Exception err)
        {
            string path = _repertoireApplication + @"\erreur";
            try
            {
                StreamWriter writer = new StreamWriter(path, true);
                writer.WriteLine("Date :" + DateTime.Now + " Message " + err.Message);
                writer.Close();
            }
            catch (Exception IOerr)
            {
                throw new NotImplementedException();
            }
        }               
    }
}
