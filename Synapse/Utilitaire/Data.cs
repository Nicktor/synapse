﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Synapse.Metiers;

namespace Synapse.Utilitaire
{
    class Data
    {
        private static List<projet> _collectionProjet;
        private static List<intervenant> _collectionIntervenant;

        public static List<intervenant> CollectionIntervenant
        {
            get
            {
                if (_collectionIntervenant == null)
                {
                    _collectionIntervenant = (List<intervenant>)Utilitaires.ChargeDonnees("intervenant");
                    if (_collectionIntervenant == null)
                        _collectionIntervenant = new List<intervenant>();                   
                }
                return _collectionIntervenant;
            }
            set { _collectionIntervenant = value; }
        }

        public static List<projet> CollectionProjet
        {
            get
            {
                if (_collectionProjet == null)
                {
                    _collectionProjet = (List<projet>)Utilitaires.ChargeDonnees("projet");
                    if (_collectionProjet == null)
                        _collectionProjet = new List<projet>();
                }
                return _collectionProjet;
            }
            set { _collectionProjet = value; }
        }

        public static void SauvegardeDonnees()
        {
            Utilitaires.SauvegarderDonnees("Intervenant", _collectionIntervenant);
            Utilitaires.SauvegarderDonnees("Projet", _collectionProjet);
        }

        public static void ChargeDeDonnees()
        {
            _collectionProjet = (List<projet>)Utilitaires.ChargeDonnees("projet");
            _collectionIntervenant = (List<intervenant>)Utilitaires.ChargeDonnees("intervenant");
        }
      }
}
